require 'me.plugins'
require 'me.globals'
require 'me.options'
require 'me.keymaps'
require 'me.telescope'
require 'me.completions'
require 'me.colorizer'
require 'me.treesitter'
require 'me.sonokai'
require 'me.lsp'


