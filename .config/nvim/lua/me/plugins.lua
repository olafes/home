-- automatically install packer
local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
  fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
  vim.cmd [[packadd packer.nvim]]
end

-- automatically sync after modifying plugins.lua
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

return require('packer').startup(function(use)
  -- packer
  use 'wbthomason/packer.nvim'

  -- a lot of plugin depend on this
  use 'nvim-lua/plenary.nvim'

  -- theme
  use 'sainnhe/sonokai'
  
  -- telescope
  use {
    'nvim-telescope/telescope-fzf-native.nvim', 
    run = 'make' 
  }
  use {
    'nvim-telescope/telescope.nvim',
    tag = '0.1.1',
    requires = {{'nvim-lua/plenary.nvim'}}
  }

  -- luasnip
  use 'L3MON4D3/LuaSnip'

  -- cmp
  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-path'
  use 'hrsh7th/cmp-nvim-lsp'
  use 'saadparwaiz1/cmp_luasnip'

  -- colorizer
  use 'norcalli/nvim-colorizer.lua'

  -- treesitter
  use {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate'
  }

  -- lsp
  use 'neovim/nvim-lspconfig'
end)
