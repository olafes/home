require'nvim-treesitter.configs'.setup {
  ensure_installed = { 'c', 'lua', 'vim', 'help', 'query', 'rust', 'typescript'},
  sync_install = false,
  auto_install = true,
  highlight = {
    enable = false,
    additional_vim_regex_highlighting = false,
  }
}
