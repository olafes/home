vim.o.termguicolors = true
vim.o.smartcase = true
vim.o.tabstop = 2
vim.o.shiftwidth = 2
vim.o.expandtab = true
vim.o.number = true
vim.o.relativenumber = true
vim.o.numberwidth = 2
vim.opt.completeopt = {'menu', 'menuone', 'noselect'}

