local cmp = require 'cmp'
local select_opts = {behavior = cmp.SelectBehavior.Select}
cmp.setup {
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end
  },
  sources = {
    {
      name = 'nvim_lsp',
      entry_filter = function(entry, ctx) 
        if entry:get_kind() == 15 then
          return false
        end

        return true
      end
    },
    {name = 'path'},
    {name = 'buffer', keyword_length = 3}
  },
  window = {
    completion = {
      winhighlight = 'Normal:Pmenu,FloatBorder:Pmenu,Search:None',  
      col_offset = -3,
      side_padding = 0,
    },
    documentation = cmp.config.window.bordered(),
  },
  formatting = {
    fields = {'kind', 'abbr', 'menu'},
    format = function(entry, item)
      return item
    end
  },
  mapping = {
    ['<C-b>'] = cmp.mapping.select_prev_item(select_opts),
    ['<C-n>'] = cmp.mapping.select_next_item(select_opts),
    ['<CR>'] = cmp.mapping.confirm({select = true}),
  }
}
