set -U fish_color_normal brwhite
set -U fish_color_command brmagenta
set -U fish_color_autosuggestion brblack
set -U fish_color_param bryellow
set -U fish_color_option brblue
set -U fish_color_quote green
set -U fish_color_error red
set -U fish_color_redirection -o yellow
set -U fish_color_end cyan
set -U fish_color_comment brblack

set -U fish_color_keyword -o yellow


set -U fish_color_match normal
set -U fish_color_selection normal
set -U fish_color_search_match normal
set -U fish_color_history_current normal
set -U fish_color_operator normal
set -U fish_color_escape normal
set -U fish_color_cwd normal
set -U fish_color_cwd_root normal
set -U fish_color_valid_path normal
set -U fish_color_user normal
set -U fish_color_host normal
set -U fish_color_cancel normal
set -U fish_pager_color_prefix normal
set -U fish_pager_color_progress normal
set -U fish_pager_color_completion normal
set -U fish_pager_color_description normal
set -U fish_pager_color_selected_background normal 
set -U fish_pager_color_selected_description normal 
set -U fish_pager_color_selected_completion normal
set -U fish_color_host_remote normal
set -U fish_pager_color_secondary_prefix normal
set -U fish_pager_color_selected_prefix normal
set -U fish_pager_color_background normal
set -U fish_pager_color_secondary_description normal
set -U fish_pager_color_secondary_completion normal
set -U fish_pager_color_secondary_background normal

