function fish_prompt
  set -l git_branch (git rev-parse --abbrev-ref HEAD 2>/dev/null)

  if test "$PWD" = "$fish_prompt_pwd" -a "$git_branch" = "$fish_prompt_git_branch"
    return
  end

  printf "\n"
  if test "$git_branch"
    set -l git_root (git rev-parse --show-toplevel | path basename)
    set -l git_prefix (git rev-parse --show-prefix)
 
    set_color -o green
    printf "%s" $git_root
    set_color normal
    set_color -d white
    printf " on "
    set_color normal
    set_color -o green
    printf "%s" $git_branch
    set_color normal
    set_color brcyan
    printf " %s\n" (path normalize '/'$git_prefix)
    
    set_color normal
  else
    set_color brcyan
    printf "%s\n" (dirs)
    set_color normal
  end

  set -g fish_prompt_pwd $PWD
  set -g fish_prompt_git_branch $git_branch
end
